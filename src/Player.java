

/**
 * 
 * Player class for the player that is me not the robot 
 * 
 * everything that is relevant to the player should go here 
 * 
 * so yeah
 * 
 * cool
 * 
 * @author maria
 *
 */
public class Player
{

	// default name for player. Maybe you could make them insert one later on
	
	//Ask Plante what is the use of defaulName 
	
	String defaultName = "Alex";
	String myName;
	int myNumberWins;
	boolean myAmAI;
	int hand;

	//constructor 
	public Player(String name)
	{

	}

	
	/**
	 * maybe I could 
	 * do it this way:
	 * if my name is not null then return myname, 
	 * 	else return defaultName 
	 * 
	 * I am still unsure about the not null part but Eclipse yelled at me
	 * 
	 * learn how to use exceptions again
	 * @return
	 */
	boolean validateName()
	{
		if (myName != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int incrementNumberWins()
	{
		return myNumberWins;

	}

	public String toString()
	{
		return defaultName;

	}
	
	public void clone(Object o) 
	{
		
	}
	
	
	//I made the hand an int but I am unsure if it should be an enum yet
	public int getHand()
	{
		return hand;
	}
	
	public String getName()
	{
		return myName; 
	}
	
	public int getNumberWins()
	{
		return myNumberWins;
	}
	
	public boolean getAmAI()
	{
		return myAmAI;
	}
	
}
